package com.fujitsu.kagamilibrary;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.fujitsu.global.finplex.nexessary.nli.nappsmartplugin.views.fragments.MirrorCameraFragment;
import com.fujitsu.kagamilibrary.R;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setCameraFragment(true);
    }

    private void setCameraFragment(boolean useFrontCamera) {
        MirrorCameraFragment mirrorCameraFragment = MirrorCameraFragment.newInstance();
        mirrorCameraFragment.setUseFrontCamera(useFrontCamera);
        getSupportFragmentManager().beginTransaction().replace(R.id.frame, mirrorCameraFragment, "camera").commit();
    }
}
